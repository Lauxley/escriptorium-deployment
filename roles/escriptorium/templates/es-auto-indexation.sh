#!/bin/bash -e

docker run --rm --name {{ escriptorium.name }}-index --link {{ escriptorium.name }}-db {% for key, value in escriptorium_env_base.items() %}--env {{ key }}={{ value }} {% endfor %}{% for key, value in escriptorium_env_es.items() %}--env {{ key }}={{ value }} {% endfor %} registry.gitlab.com/scripta/escriptorium:{{ escriptorium.version }} /usr/local/bin/python manage.py index
