from escriptorium.settings import *

{% if escriptorium.data is defined and escriptorium.data.nfs.local_driver %}
STATIC_ROOT = '{{ escriptorium.data.dir }}/static'
MEDIA_ROOT = '{{ escriptorium.data.dir }}/media'
{% else %}
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
{% endif %}
