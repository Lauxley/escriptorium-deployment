---
- name: Build nginx conf dirs
  file:
    path: "{{ item }}"
    state: directory
    mode: 0644
  with_items:
    - "{{ escriptorium.dir }}/nginx/conf.d"

- name: Build nginx.conf
  template:
    src: nginx.conf
    dest: "{{ escriptorium.dir }}/nginx/nginx.conf"
    mode: 0644
    owner: root
  notify: restart nginx

- name: Build nginx escriptorium.conf
  template:
    src: escriptorium.conf
    dest: "{{ escriptorium.dir }}/nginx/conf.d/escriptorium.conf"
    mode: 0644
    owner: root
  notify: restart nginx

- name: Build shared environment variables
  set_fact:
    escriptorium_env_base:
      SECRET_KEY: "{{ escriptorium.django_secret_key }}"
      SQL_ENGINE: django.db.backends.postgresql
      SQL_HOST: "{{ escriptorium.name }}-db"
      SQL_PORT: "5432"
      DJANGO_SETTINGS_MODULE: escriptorium.docker_settings
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: "{{ escriptorium.db_root_password }}"
      POSTGRES_DB: escriptorium
      DATABASE: postgres
      DJANGO_SU_NAME: admin
      DJANGO_SU_PASSWORD: "{{ escriptorium.django_admin_password }}"
      CSRF_TRUSTED_ORIGINS: "https://{{ escriptorium.public_hostname }}"
      REDIS_HOST: "{{ escriptorium.name }}-redis"
      TEXT_ALIGNMENT: "{{ escriptorium.text_alignment_enabled|default(False)|ternary('true', 'false') }}"
      USE_X_FORWARDED_HOST: "True"
      VERSION_DATE: "{{ escriptorium.version }}"
      LANG: "C.UTF-8"
      OPENBLAS_NUM_THREADS: "1"

    escriptorium_env_mail:
      EMAIL_HOST: "{{ escriptorium.name }}-mail"
      DJANGO_FROM_EMAIL: "{{ escriptorium.django_from_email | default('noreply@mydomain.com') }}"
      DJANGO_SU_EMAIL: "{{ escriptorium.django_su_email | default('admin@admin.com') }}"

    escriptorium_env_workers:
      KRAKEN_TRAINING_LOAD_THREADS: "8"
      CELERY_MAIN_CONC: "4"
      CELERY_LOW_CONC: "4"

      # Special setting to avoid cpu hijacking and thread explosion
      # https://gitlab.com/scripta/escriptorium/-/issues/444#note_690277351
      OMP_NUM_THREADS: "1"

    # All the services from which backend & workers depend
    escriptorium_dep_services:
      - "{{ escriptorium.name }}-db"
      - "{{ escriptorium.name }}-redis"
      - "{{ escriptorium.name }}-mail"

    # Elasticsearch is optional
    escriptorium_env_es:
      DISABLE_ELASTICSEARCH: "true"
    escriptorium_es_services: []

    escriptorium_env_export: {}

    escriptorium_sentry: []

  tags:
    - app
    - workers
    - migrate
    - elasticsearch

- name: Build elasticsearch environment variables
  set_fact:
    escriptorium_env_es:
      DISABLE_ELASTICSEARCH: "false"
      ELASTICSEARCH_URL: "http://{{ escriptorium.name }}-es:9200"

    escriptorium_es_services:
      - "{{ escriptorium.name }}-es"
  tags:
    - app
    - workers
    - elasticsearch
  when: escriptorium.search.enabled

- name: Build mail environment variables
  set_fact:
    escriptorium_env_mail:
      EMAIL_HOST: "{{ escriptorium.email_host }}"
      DJANGO_FROM_EMAIL: "{{ escriptorium.django_from_email | default('noreply@' + public_hostname) }}"
      DJANGO_SU_EMAIL: "{{ escriptorium.django_su_email | default('admin@admin.com') }}"

    # Email service is not available in this case
    escriptorium_dep_services:
      - "{{ escriptorium.name }}-db"
      - "{{ escriptorium.name }}-redis"
  when: escriptorium.email_host is defined
  tags:
    - app
    - workers

- name: Build export environment variable
  set_fact:
    escriptorium_env_export:
      EXPORT_TEI_XML: "true"
      EXPORT_OPENITI_MARKDOWN: "true"
  when: escriptorium.export_openiti
  tags:
    - app
    - workers

- name: Build sentry environment variable
  set_fact:
    escriptorium_sentry:
      SENTRY_DSN: "{{ escriptorium.sentry.dsn }}"
  when: escriptorium.sentry.enabled
  tags:
    - app
    - workers

- name: Build nginx content security policy
  set_fact:
    nginx_header_content_security_policy: "{{ escriptorium.nginx_header_content_security_policy }}"
  tags: nginx

- name: Build nginx container common labels for domain_name + www subdomain
  set_fact:
    # Interpolate dictionary to use variables in keys
    nginx_labels: "{{
      {
        'traefik.enable': 'true',
        'traefik.http.routers.' + escriptorium.name + '.rule': 'Host(`' + escriptorium.public_hostname + '`) || Host(`www.' + escriptorium.public_hostname + '`)',
        'traefik.http.routers.' + escriptorium.name + '.tls': 'true',
        'traefik.http.routers.' + escriptorium.name + '.tls.certresolver': 'teklia',
        'traefik.http.services.' + escriptorium.name + '.loadbalancer.server.port': '8080',
        'traefik.http.routers.' + escriptorium.name + '.middlewares': escriptorium.name + '-security-header',
        'traefik.http.middlewares.' + escriptorium.name + '-security-header.headers.stsSeconds': '15768000',
        'traefik.http.middlewares.' + escriptorium.name + '-security-header.headers.forceSTSHeader': 'true',
        'traefik.http.middlewares.' + escriptorium.name + '-security-header.headers.contentSecurityPolicy': nginx_header_content_security_policy
      }
    }}"
  tags: nginx
  when: escriptorium.use_www

- name: Build nginx container common labels for domain name only
  set_fact:
    # Interpolate dictionary to use variables in keys
    nginx_labels: "{{
      {
        'traefik.enable': 'true',
        'traefik.http.routers.' + escriptorium.name + '.rule': 'Host(`' + escriptorium.public_hostname + '`)',
        'traefik.http.routers.' + escriptorium.name + '.tls': 'true',
        'traefik.http.routers.' + escriptorium.name + '.tls.certresolver': 'teklia',
        'traefik.http.services.' + escriptorium.name + '.loadbalancer.server.port': '8080',
        'traefik.http.routers.' + escriptorium.name + '.middlewares': escriptorium.name + '-security-header',
        'traefik.http.middlewares.' + escriptorium.name + '-security-header.headers.stsSeconds': '15768000',
        'traefik.http.middlewares.' + escriptorium.name + '-security-header.headers.forceSTSHeader': 'true',
        'traefik.http.middlewares.' + escriptorium.name + '-security-header.headers.contentSecurityPolicy': nginx_header_content_security_policy
      }
    }}"
  tags: nginx
  when: not escriptorium.use_www

- name: Build backup script
  template:
    src: backup_escriptorium.sh
    dest: "/usr/local/bin/backup-{{ escriptorium.name }}"
    owner: root
    group: root
    mode: 0700
  tags: backups
  when: backup_client.enabled

- name: Setup escriptorium config
  template:
    src: docker_settings.py
    dest: "{{ escriptorium.dir }}/docker_settings.py"
    mode: 0644

- name: NFS data volume mount
  docker_volume:
    driver: local
    driver_options:
      type: nfs
      o: "{{ escriptorium.data.nfs.options }}"
      device: "{{ escriptorium.data.nfs.device }}"
    volume_name: "{{ escriptorium.data.nfs.volume_name }}"
  when: escriptorium.data.nfs.local_driver

- import_tasks: containers.yml
  when: not docker.swarm is defined or not docker.swarm.enabled

- import_tasks: services.yml
  when: docker.swarm is defined and docker.swarm.enabled and docker.swarm.manager
